{...}: {
  imports = [
    ./hyprland.nix
    ./hyprcursor.nix
    ./hypridle.nix
    ./hyprlock.nix
    ./hyprpaper.nix
    ./xdph.nix
  ];
}
