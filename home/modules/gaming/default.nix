{
  config,
  lib,
  pkgs,
  inputs,
  osConfig,
  ...
}: let
  nixpkgs-olympus = inputs.nixpkgs-olympus.legacyPackages.${pkgs.system};
  nixpkgs-sgdboop = inputs.nixpkgs-sgdboop.legacyPackages.${pkgs.system};
  nix-gaming = inputs.nix-gaming.packages.${pkgs.system};
  gpuType =
    if osConfig.cfg.gpu.nvidia.enable
    then "nvidia"
    else if osConfig.cfg.gpu.amd.enable
    then "amd"
    else "full"; # Fallback in case neither is enabled
in {
  options.cfg.gaming.enable = lib.mkOption {
    type = lib.types.bool;
    default = false;
    description = "Enable gaming packages and configuration.";
  };

  config = lib.mkIf config.cfg.gaming.enable {
    home.packages = with pkgs; [
      (prismlauncher.override {
        jdks = [
          temurin-jre-bin-8
          temurin-jre-bin-17
          temurin-jre-bin-21
        ];
      })
      (gamescope.overrideAttrs (_: {
        # NOTE: https://github.com/ValveSoftware/gamescope/issues/1622#issuecomment-2508182530
        NIX_CFLAGS_COMPILE = ["-fno-fast-math"];
      }))
      lutris
      cemu
      heroic
      nvtopPackages.${gpuType}
      nixpkgs-olympus.olympus
      nixpkgs-sgdboop.sgdboop
      nix-gaming.osu-lazer-bin
    ];
    xdg = {
      dataFile = {
        "lutris/runners/proton/GE-Proton" = {
          source = pkgs.proton-ge-bin.steamcompattool;
        };
      };
    };
  };
  imports = [
    ./mangohud
  ];
}
